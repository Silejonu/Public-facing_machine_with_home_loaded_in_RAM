# Configurer des machines Linux accessibles au public avec un répertoire personnel chargé en RAM

Dans ce guide, nous verrons comment configurer un système Linux à destination de machines accessibles au public, de façon sécurisée. Ce guide est particulièrement utile pour des EPN (bibliothèques, cyber-cafés, Cyber-Base…), où un grand nombre d’utilisateurs se partagent le même ordinateur et est amené à manipuler des fichiers personnels.

# Sommaire

- [Quels sont les avantages de cette méthode ?](#quels-sont-les-avantages-de-cette-méthode)
- [Comment ça marche ?](#comment-ça-marche)
	- [Du point de vue de l’utilisateur](#du-point-de-vue-de-l-utilisateur)
	- [Du point de vue de l’administrateur](#du-point-de-vue-de-l-administrateur)
	- [D’un point de vue technique](#d-un-point-de-vue-technique)
- [Choix de la distribution](#choix-de-la-distribution)
- [Installation du système de base](#installation-du-système-de-base)
- [Configuration du système](#configuration-du-système)
	- [Gestion des paquets](#gestion-des-paquets)
	- [Création de la session utilisateur](#création-de-la-session-utilisateur)
	- [Contournements](#contournements)
	- [Paramètrage de la confidentialité](#paramètrage-de-la-confidentialité)
	- [Configuration de Firefox](#configuration-de-firefox)
	- [Customisation du bureau](#customisation-du-bureau)
	- [Création du modèle de répertoire personnel](#création-du-modèle-de-répertoire-personnel)
		- [Amélioration du script de maintenance](#amélioration-du-script-de-maintenance)
- [Aller plus loin](#aller-plus-loin)

## Quels sont les avantages de cette méthode ?

- aucune donnée personnelle n’est écrite sur le disque : elles sont définitivement effacées à l’extinction de l’ordinateur, sans possibilité de les récupérer
- quoi que les utilisateurs fassent, la session reviendra toujours à l'état initial (que vous avez choisi) après un redémarrage
- fonctionne sur du matériel modeste
- peu de maintenance requise
- peut être utilisé par des personnes non-technophiles
- facile à mettre en place
- ne nécissite rien de plus que des outils Linux de base
- coût réduit

## Comment ça marche ?

### Du point de vue de l’utilisateur

L'utilisateur allume l’ordinateur, fait ce qu’il a à faire, télécharge autant de fichiers personnels et se connecte à autant de comptes qu’il le souhaite. Lorsqu’il a terminé, il éteint simplement l’ordinateur, qui « oublie » instantanément tous ses comptes et fichiers personnels, rendant impossible le vol de ceux-ci.

### Du point de vue de l’administrateur

Vous vous connectez à votre session administrateur, configurez le système comme vous souhaitez qu’il apparaisse pour vos utilisateurs, et lancez `maintenance.sh`. Tous vos réglages seront appliqués à la session de vos utilisateurs.  
Tous les changements que vos utilisateurs font seront perdus lorsqu’ils éteindront la machine. Qu’ils aient changé le fond d’écran, supprimé le profil Firefox, ou activé des options farfelues, vous n’en serez pas affecté, puisque tout aura disparu après un redémarrage !

### D’un point de vue technique

Cette méthode tire partie de `/etc/skel` et `mkhomedir_helper`.  
Vous créerez un utilisateur générique dont le répertoire personnel sera monté en RAM (`/dev/shm`). De ce fait, le contenu de `$HOME` ne sera jamais écrit sur le disque, et ne survivra pas à une extinction.  
Nous créerons un modèle dans `/etc/skel`, qui sera poussé vers le répertoire utilisateur par `mkhomedir_helper` à chaque démarrage.

## Choix de la distribution

Dans ce guide, nous utiliserons [CentOS Stream](https://www.centos.org). J’ai fait ce choix car c’est une distribution stable, qui fournit la dernière version en date de Firefox ESR.

Je préfère utiliser Firefox ESR pour la stabilité que cette version procure, et afin de profiter du plein potentiel des [paramètres d’entreprise](https://github.com/mozilla/policy-templates) (comme configurer le moteur de recherche, par exemple).

Toutefois, vous êtes libre d’utiliser la distribution de votre choix. En gardant en tête une limitation technique : les Snaps ne fonctionneront pas avec notre configuration. Si vous choisissez d’utiliser Ubuntu comme base, vous devrez remplacer les paquets Snap par leur équivalent .deb (pour Firefox, par exemple, il faudra ajouter un PPA ou l’installer manuellement).

Ce guide présentera les commandes pour CentOS Stream 9. Si vous utilisez une autre distribution, vous devrez en adapter certaines. Cela dit, les objectifs de chaque commande sont clairement présentés, et vous ne devriez pas avoir de mal à les adapter à la distribution de votre choix.

## Installation du système de base

- *Sélection Logiciel :* sélectionnez le modèle « Workstation ».
- *Mot de passe administrateur :* passez, nous ne voulons pas de compte root.
- *Création Utilisateur :* cochez « Faire de cet utilisateur un administrateur », et utilisez un mot de passe fort. Nous appelerons le notre « Administrateur ».
- *Installation Destination :* créez une partition de démarrage (512 Mo est amplement suffisant) et montez la sur `/boot/efi` pour de l’UEFI, ou `/boot` pour du BIOS « Legacy ». Utilisez l’espace restant pour la partition `/`. Choisissez EXT4 pour le système de fichiers, et n’activez pas le chiffrement. Nous n’en avons pas besoin puisqu’aucune donnée utilisateur ne sera écrite sur le disque, et nous voulons que les utilisateurs puissent accéder au système sans taper de mot de passe. **Ne créez pas de partition swap** : nous ne voulons pas que des fichiers soient inscrits sur le disque.

## Configuration du système

Il est fortement recommandé que vous suiviez ce guide pas-à-pas, et que vous apportiez des modifications selon vos besoins. Cela dit, **si vous avez déjà lu ce guide**, et que les paramètres par défaut vous conviennent, vous pouvez utiliser [ce script de configuration automatisée](https://codeberg.org/Silejonu/Public-facing_machine_with_home_loaded_in_RAM/src/branch/main/Lazy%20installation%20script/install_public_facing_machine.sh). *Attention, ce script appliquera des paramètres d’entreprise Firefox prévus pour des locuteurs d’anglais américain. Pour appliquer les paramètres de localisation française présentés sur cette page, lancez cette commande (après avoir lancé le script) :*

```bash
sudo wget 'https://codeberg.org/Silejonu/Public-facing_machine_with_home_loaded_in_RAM/raw/branch/main/localisations/Fran%C3%A7ais/policies.json' --directory-prefix /etc/firefox/policies
```

### Gestion des paquets

Commençons par activer les dépôts dont nous aurons besoin.

```bash
# Activer CodeReady Linux Builder
sudo dnf config-manager --set-enabled crb
# Activer EPEL
sudo dnf install -y epel-release epel-next-release
# Activer RPM Fusion free & nonfree
sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %rhel).noarch.rpm
sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %rhel).noarch.rpm
# Mettre à jour les paquets
sudo dnf upgrade -y
```

Ensuite, ajoutons quelques logiciels que nos utilisateurs sont susceptibles d’utiliser (adaptez cette liste à vos besoins) :

```bash
# Codecs vidéo pour Firefox
sudo dnf install -y ffmpeg gstreamer1-libav
# Paquets multimédias nécessaires pour les applications gstreamer
sudo dnf groupupdate -y multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
# Paquets complémentaires sons & vidéo nécessaires pour certains logiciels
sudo dnf groupupdate -y sound-and-video
# LibreOffice
sudo dnf install -y libreoffice-{calc,draw,impress,writer}
cp /usr/share/applications/libreoffice-startcenter.desktop ~/.local/share/applications/
sed -i '/NoDisplay=true/d' ~/.local/share/applications/libreoffice-startcenter.desktop
# GIMP
sudo dnf install -y gimp
# Support du système de fichiers des appareils iOS
sudo dnf install -y ifuse
# Support des images HEIF
sudo dnf install -y libheif
xdg-mime default org.gnome.eog.desktop image/heic
xdg-mime default org.gnome.eog.desktop image/heif
# Support des images WEBP
sudo dnf install -y webp-pixbuf-loader
xdg-mime default org.gnome.eog.desktop image/webp
```

Enfin, supprimons quelques paquets dont nous n’avons pas besoin :

```bash
# Flatpak, car nous ne voulons pas que les utilisateurs installent des logiciels eux-mêmes
sudo dnf remove -y flatpak
# Un client e-mail n’a aucun intérêt sur une machine partagée
sudo dnf remove -y evolution
# Nous ne voulons pas des mises à jour automatiques et utiliserons uniquement dnf pour la gestion des paquets
sudo dnf remove -y PackageKit*
# Cockpit est un utilitaire pour serveurs
sudo dnf remove -y cockpit
# Visite guidée est inutile dans notre cas
sudo dnf remove -y gnome-tour
```

### Création de la session utilisateur

Premièrement, décidons du nom de notre utilisateur générique :

```bash
# Son nom complet
fullname=Utilisateur
# Son nom d’utilisateur (minuscules uniquement)
username=utilisateur
```

```bash
# Nous créons notre utilisateur générique, avec son répertoire utilisateur monté en RAM
sudo useradd ${username} --home-dir /dev/shm/${username} --shell /bin/bash --comment "${fullname}"
```

Nous n’avons pas besoin de connaître le mot de passe de notre utilisateur. Ce serait même contre-productif, puisque se re-connecter à sa session créerait un répertoire utilisateur vierge, comme si nous venions d’installer le système, et ignorerait le modèle que nous avons créé (nous reparlerons plus loin du modèle du répertoire personnel). Nous allons donc générer un mot de passe aléatoirement, inconnu de tous (nous compris), et activerons la connexion automatique pour notre utilisateur générique. La seule façon de s’y connecter sera de redémarrer l’ordinateur. C’est ce que nous voulons.

```bash
# Génération d’un mot de passe aléatoire
user_pw=$(printf "$(date)${RANDOM}" | md5sum | cut -d' ' -f1)
# Assignation de ce mot de passe à notre utilisateur
echo "${username}:${user_pw}" | sudo chpasswd
# Désactivation de la possibilité pour les utilisateurs de changer leur mot de passe (seul le super-utilisateur peut le faire)
sudo chmod u-s /usr/bin/passwd
# Activation de la connexion automatique
sudo sed -i "/\[daemon\]/a AutomaticLogin=${username}" /etc/gdm/custom.conf
sudo sed -i "/\[daemon\]/a AutomaticLoginEnable=True" /etc/gdm/custom.conf
# Nous cachons l’utilisateur de la page de connexion
sudo tee /var/lib/AccountsService/users/${username} << EOF > /dev/null
[User]
SystemAccount=true
EOF
```

Maintenant nous devons créer le service qui dupliquera le contenu de notre modèle dans le répertoire de notre utilisateur générique :

```bash
sudo tee /etc/systemd/system/mkhomedir_helper.service << EOF > /dev/null
[Unit]
Description=Lancer mkhomedir_helper pour générer le répertoire personnel de l’utilisateur « ${username} » à partir du modèle skel.

[Service]
ExecStart=/usr/sbin/mkhomedir_helper ${username}

[Install]
WantedBy=graphical.target
EOF
```

SELinux empêchera `mkhomedir_helper` d’écrire dans le répertoire personnel de notre utilisateur générique, nous allons donc l’y autoriser en créant une règle personnalisée :

```bash
# Nous créons le modèle de la règle
sudo tee /tmp/custom_mkhomedir_helper.te << EOF > /dev/null
module custom_mkhomedir_helper 1.0;

require {
	type oddjob_mkhomedir_t;
	type tmpfs_t;
	class dir { create setattr };
	class file { create open setattr write };
}

#============= oddjob_mkhomedir_t ==============
allow oddjob_mkhomedir_t tmpfs_t:dir { create setattr };
allow oddjob_mkhomedir_t tmpfs_t:file { create open setattr write };
EOF
# Nous compilons le module
sudo checkmodule -M -m -o /tmp/custom_mkhomedir_helper.mod /tmp/custom_mkhomedir_helper.te
sudo semodule_package -m /tmp/custom_mkhomedir_helper.mod -o /tmp/custom_mkhomedir_helper.pp
# Nous installons le module
sudo semodule -i /tmp/custom_mkhomedir_helper.pp
```

Il ne reste plus qu’à activer le service :

```bash
sudo systemctl enable mkhomedir_helper
```

### Contournements

Les paramètres suivants servent à empêcher les utilisateurs de s’enfermer hors de leur session :

```bash
# Désactive la possibilité de mettre en veille l’ordinateur
sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target
# Désactive la possibilité de verrouiller l’écran
gsettings set org.gnome.desktop.lockdown disable-lock-screen true
# Désactive le verrouillage automatique de l’écran
gsettings set org.gnome.desktop.screensaver lock-enabled false
# Supprime le délai de verrouillage automatique de l’écran
gsettings set org.gnome.desktop.session idle-delay 0
```

Étant donné que nous n’avons pas de partition swap, cela peut être une bonne idée de compresser les fichiers en RAM avec zram :

```bash
sudo dnf install -y zram-generator
sudo tee /etc/systemd/zram-generator.conf << EOF > /dev/null
[zram0]
zram-size = ram / 2
compression-algorithm = zstd
swap-priority = 100
fs-type = swap
EOF
```

### Paramètrage de la confidentialité

```bash
# Désactive l’historique d’utilisation des applications
gsettings set org.gnome.desktop.privacy remember-app-usage false
# Désactive les fichiers récents
gsettings set org.gnome.desktop.privacy remember-recent-files false
# Supprime les fichiers temporaires automatiquement
gsettings set org.gnome.desktop.privacy remove-old-temp-files true
# Supprime les fichiers dans la corbeille automatiquement
gsettings set org.gnome.desktop.privacy remove-old-trash-files true
# Règle la suppression automatique sur 1 jour
gsettings set org.gnome.desktop.privacy old-files-age 1
```

### Configuration de Firefox

Les [paramètres d’entreprise de Firefox](https://github.com/mozilla/policy-templates) vous permettrons d’améliorer la sécurité et de personnaliser l’expérience de vos utilisateurs.

```bash
# Créons le répertoire qui va accueillir les paramètres d’entreprise
sudo mkdir -p /etc/firefox/policies
# Et téléchargeons-les
sudo wget 'https://codeberg.org/Silejonu/Public-facing_machine_with_home_loaded_in_RAM/raw/branch/main/localisations/Fran%C3%A7ais/policies.json' --directory-prefix /etc/firefox/policies
```

J’ai créé [un modèle](https://codeberg.org/Silejonu/Public-facing_machine_with_home_loaded_in_RAM/src/branch/main/localisations/Fran%C3%A7ais/policies.json) qui devrait convenir à la majorité des cas, mais je vous recommande de parcourir les options du fichier avant de l’utiliser. À minima, jetez un coup d’œil aux paramètres de localisation :

- ajoutez les listes de blocage de vos locales dans les paramètres d’uBlock Origin (`selectedFilterLists`)
- ajoutez ou modifiez vos locales à la liste `"RequestedLocales": "fr-FR,fr,en-US,en"`
- modifiez [le pack de langue utilisé](https://addons.mozilla.org/firefox/language-tools/) en suivant [ces instructions](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#extensionsettings), si le pack français ne vous convient pas.

J’ai choisi Startpage comme moteur de recherche par défaut, parce qu’il renvoie les résultats de Google, mais anonymisés. Contrainement à Google, il ne bloque pas la recherche derrière une bannière « Avant de continuer ». Cela m’est apparu particulièrement intéressant sur un système qui ne sauvegarde pas les cookies. Vous pouvez [modifier les paramètres de Startpage](https://www.startpage.com/do/settings) puis copier l’URL des paramèters dans `URLTemplate`. Cela devrait ressembler à ceci :

```json
          "Name": "Startpage",
          "URLTemplate": "https://www.startpage.com/sp/search?query={searchTerms}&prfe=8d48ca6adae01eb75c25333c67841ceb81a522c0de4ff281a3d9405ccea07ab8d449902ec838dee3a9976bb0c676736d7ea3f0dc440f9f160da1d13091f58946f2565b014a377f9e300c62677d68&language=francais&t=device&lui=francais&cat=web&sc=4f9emuRQYhFa20&abp=-1",
          "IconURL": "https://www.startpage.com/sp/cdn/favicons/favicon--default.ico"
        }
```

J’ai choisi [Cloudflare for Families](https://developers.cloudflare.com/1.1.1.1/setup/#dns-over-https-doh) pour le DNS over HTTPS parce que cela bloque les domaines de logiciels malveillants et contenu adulte, et il me semble que c’est ce dont la plupart des personnes voudraient pour leurs machines publiques. Si vous préfèreriez un DNS respectueux de la vie privée, jetez un coup d’œil à [Quad9](https://quad9.net).

Vous voudrez probablement changer l’`URL` de la page d’accueil (`Homepage`), et peut-être ajouter quelques marque-pages avec `ManagedBookmarks`.

uBlock Origin (et de nombreuses autres extentions) peuvent être configurées au travers du fichier `policies.json`. Consultez [sa documentation](https://github.com/gorhill/uBlock/wiki/Deploying-uBlock-Origin) pour plus d’informations.

### Customisation du bureau

Les configurations suivantes ne sont pas strictement nécessaires, mais mon expérience a prouvé qu’elles avaient leur utilité :

```bash
# Supprime le répertoire « Public »
rmdir $(xdg-user-dir PUBLICSHARE)
# Affiche le jour de la semaine dans la barre supérieure
gsettings set org.gnome.desktop.interface clock-show-weekday true
# Défini les favoris
gsettings set org.gnome.shell favorite-apps "['firefox.desktop', 'org.gnome.Nautilus.desktop', 'libreoffice-startcenter.desktop']"
```

### Création du modèle de répertoire personnel

Nous y sommes presque ! Tout ce qu’il nous reste à faire est de créer le modèle qui sera monté en RAM lorsque notre utilisateur générique se connectera.  
Pour cela, nous allons créer un script qui va se charger de toute la maintenance de base de notre système :

```bash
sudo tee /usr/local/bin/maintenance.sh << EOF > /dev/null
#!/usr/bin/bash

if [[ \${UID} == 0 ]] ; then
  echo 'Erreur : Ce script ne doit pas être lancé avec les privilèges du super-utilisateur.'
  exit 2
fi

# Mise à jour des paquets
sudo dnf update -y

# Les filtres d’uBlock Origin ne peuvent pas être mis à jour automatiquement, alors faisons-le manuellement
notify-send --hint int:transient:1 'uBlock Origin' 'Videz tous les caches et mettez à jour les filtres.'
firefox

# Attente de la clôture de Firefox avant de continuer
wait \$(pidof firefox)
# Par précaution, un peu plus d’attente (sinon il arrive que des fichiers échouent à être synchronisés)
sleep 30

# Copie du contenu de la session courante vers /etc/skel
sudo rsync -avz --delete --exclude-from=/usr/local/share/rsync_ignore "\${HOME}/" /etc/skel
EOF
```

Certains fichiers ne doivent pas être ajouté au modèle du répertoire personnel pour des raisons de confidentialité/sécurité, ou simplement parce qu’il empêcheraient certains logiciels de fonctionner normalement. Pour nous simplifier la vie, nous allons créer une liste noire pour ces fichiers. Éditez cette liste en fonction de vos besoins, mais voici une base :

```bash
sudo tee /usr/local/share/rsync_ignore << EOF > /dev/null
.bash_history
.cache
.lock
.ssh
.sudo_as_admin_successful
bookmarks
lock
sessionstore-backups
Trash
EOF
```

```bash
# Rendons notre script exécutable…
sudo chmod 755 /usr/local/bin/maintenance.sh
# … et lançons-le pour la première fois
maintenance.sh
```

C’est terminé ! Vous pouvez à présent redémarrer et commencer à profiter de votre nouveau système !

N’oubliez pas de lancer `maintenance.sh` de temps en temps pour mettre à jour votre installation !  
*Évidemment vous devrez vous connecter avec l’utilisateur créé lors de l’installation initiale puisque l’utilisateur générique n’a pas de droits sudo.*

#### Amélioration du script de maintenance

Le script de maintenance que nous avons créé est parfaitement basique. Je vous recommande de l’améliorer pour automatiser quelques actions supplémentaires, surtout si vous avez beaucoup de machines. D’expérience, je vous recommande de créer un dépôt git, et d’y stocker `policies.json` ainsi que `rsync_ignore`. Ensuite, ajoutez les lignes suivantes à `/usr/local/bin/maintenance.sh` :

```bash
sudo wget {URL_vers_policies.json} --directory-prefix /etc/firefox/policies/
sudo wget {URL_vers_rsync_ignore} --directory-prefix /usr/local/share/
```

Désormais, lorsque vous aurez besoin d’apporter des modifications à ces fichiers, vous n’aurez à le faire qu’une seule fois, sur votre dépôt git, et les changements seront appliqués à toutes vos machines en lançant `maintenance.sh`. Cela réduira fortement le temps dont vous aurez besoin pour maintenir vos machines.

Dans un registre similaire, vous aurez parfois besoin de faire de petites modifications à votre système. Plutôt que de les lancer sur chaque machine individuellement, je vous recommande de créer un fichier `ajustements.sh` dans votre dépôt git, et de rajouter la ligne suivante au début de `/usr/local/bin/maintenance.sh` :

```bash
wget {URL_vers_ajustements.sh} --directory-prefix /tmp/
bash /tmp/ajustements.sh
```

Désormais, lorsque vous voudrez apporter des modifications à votre système, vous n’aurez qu’à ajouter les commandes de votre choix à `ajustements.sh` dans votre dépôt git, et elles seront lancées avec `maintenance.sh`.

## Aller plus loin

Voici quelques idées de choses que vous pourriez vouloir faire :

- [installer les polices Microsoft](https://codeberg.org/Silejonu/miscellaneous_scripts/src/branch/main/02.%20Linux/install_microsoft_fonts.sh), si vos utilisateurs ont besoin d’éditer des documents
- changer le fond d’écran pour quelque chose d’informatif (par example, vous pourriez expliquer que l’ordinateur « oublie » tout ce qui y a été fait lorsqu’il est éteint)
- ajouter le support des DVDs (`sudo dnf install rpmfusion-free-release-tainted && sudo dnf install libdvdcss`)
- ajouter un correcteur d’orthographe/grammaire plus efficace pour LibreOffice : [Grammalecte](https://grammalecte.net/)
