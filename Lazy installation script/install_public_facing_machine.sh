#!/usr/bin/bash

# Author: Silejonu
# E-mail: silejonu@tutanota.com
# Date: 2023-05-19
# Source: https://codeberg.org/Silejonu/Public-facing_machine_with_home_loaded_in_RAM

if [[ $UID == 0 ]] ; then
  echo 'Error: This script must not be ran with super-user privileges.'
  exit 2
fi

echo 'The script will create a generic user with autologging.'
read -rp "Enter its full name: " fullname
read -rp "Enter its login (lowercase only): " username

## System configuration

### Package management

# Enable CRB
sudo dnf config-manager --set-enabled crb
# Enable EPEL
sudo dnf install -y epel-release epel-next-release
# Enable RPMFusion free and nonfree
sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %rhel).noarch.rpm
sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %rhel).noarch.rpm
# Update the system
sudo dnf upgrade -y

# Video codecs for Firefox
sudo dnf install -y ffmpeg gstreamer1-libav
# Multimedia packages needed by gstreamer enabled applications
sudo dnf groupupdate -y multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
# sound-and-video complement packages needed by some applications
sudo dnf groupupdate -y sound-and-video
# LibreOffice
sudo dnf install -y libreoffice-{calc,draw,impress,writer}
cp /usr/share/applications/libreoffice-startcenter.desktop ~/.local/share/applications/
sed -i '/NoDisplay=true/d' ~/.local/share/applications/libreoffice-startcenter.desktop
# GIMP
sudo dnf install -y gimp
# iOS devices filesystem support
sudo dnf install -y ifuse
# HEIF images support
sudo dnf install -y libheif
xdg-mime default org.gnome.eog.desktop image/heic
xdg-mime default org.gnome.eog.desktop image/heif
# WEBP images support
sudo dnf install -y webp-pixbuf-loader
xdg-mime default org.gnome.eog.desktop image/webp

# Flatpak as we don't want users to install software themselves
sudo dnf remove -y flatpak
# A mail client serves no use on a shared machine
sudo dnf remove -y evolution
# We don't want automatic updates and we'll stick to dnf for package management
sudo dnf remove -y PackageKit*
# Cockpit is a server utility
sudo dnf remove -y cockpit
# Tour serves no use in our case
sudo dnf remove -y gnome-tour

# We create our generic user, with its home directory mounted in RAM
sudo useradd ${username} --home-dir /dev/shm/${username} --shell /bin/bash --comment "${fullname}"

# We generate a random password for the user
user_pw=$(printf "$(date)${RANDOM}" | md5sum | cut -d' ' -f1)
# We assign the password to our user
echo "${username}:${user_pw}" | sudo chpasswd
# We prevent anyone without sudo privileges to change their password
sudo chmod u-s /usr/bin/passwd
# We enable automatic login
sudo sed -i "/\[daemon\]/a AutomaticLogin=${username}" /etc/gdm/custom.conf
sudo sed -i "/\[daemon\]/a AutomaticLoginEnable=True" /etc/gdm/custom.conf
# We hide our user from the login screen
sudo tee /var/lib/AccountsService/users/${username} << EOF > /dev/null
[User]
SystemAccount=true
EOF

sudo tee /etc/systemd/system/mkhomedir_helper.service << EOF > /dev/null
[Unit]
Description=Run mkhomedir_helper in order to generate the home directory for the user "${username}" from the skel template.

[Service]
ExecStart=/usr/sbin/mkhomedir_helper ${username}

[Install]
WantedBy=graphical.target
EOF

# We create the rule template
sudo tee /tmp/custom_mkhomedir_helper.te << EOF > /dev/null
module custom_mkhomedir_helper 1.0;

require {
	type oddjob_mkhomedir_t;
	type tmpfs_t;
	class dir { create setattr };
	class file { create open setattr write };
}

#============= oddjob_mkhomedir_t ==============
allow oddjob_mkhomedir_t tmpfs_t:dir { create setattr };
allow oddjob_mkhomedir_t tmpfs_t:file { create open setattr write };
EOF
# We build the module
sudo checkmodule -M -m -o /tmp/custom_mkhomedir_helper.mod /tmp/custom_mkhomedir_helper.te
sudo semodule_package -m /tmp/custom_mkhomedir_helper.mod -o /tmp/custom_mkhomedir_helper.pp
# We install the module
sudo semodule -i /tmp/custom_mkhomedir_helper.pp

sudo systemctl enable mkhomedir_helper

# Disable ability to put the computer to sleep
sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target
# Disable ability to lock the screen
gsettings set org.gnome.desktop.lockdown disable-lock-screen true
# Disable automatic screen lock
gsettings set org.gnome.desktop.screensaver lock-enabled false
# Disable blank screen timer
gsettings set org.gnome.desktop.session idle-delay 0

sudo dnf install -y zram-generator
sudo tee /etc/systemd/zram-generator.conf << EOF > /dev/null
[zram0]
zram-size = ram / 2
compression-algorithm = zstd
swap-priority = 100
fs-type = swap
EOF

# Disable application usage history
gsettings set org.gnome.desktop.privacy remember-app-usage false
# Disable recent files
gsettings set org.gnome.desktop.privacy remember-recent-files false
# Automatically delete temporary files (after 30 days by default)
gsettings set org.gnome.desktop.privacy remove-old-temp-files true
# Automatically delete old trash files (after 30 days by default)
gsettings set org.gnome.desktop.privacy remove-old-trash-files true
# Set automatic deletion period to 1 day
gsettings set org.gnome.desktop.privacy old-files-age 1

# Let's create the Firefox directory that will host the policies
sudo mkdir -p /etc/firefox/policies
# And download the policies
sudo wget 'https://codeberg.org/Silejonu/Public-facing_machine_with_home_loaded_in_RAM/raw/branch/main/Configuration%20files/policies.json' --directory-prefix /etc/firefox/policies

### Desktop customisation

# Delete the ~/Public directory
rmdir $(xdg-user-dir PUBLICSHARE)
# Show the weekday in the top bar
gsettings set org.gnome.desktop.interface clock-show-weekday true
# Set the favourites
gsettings set org.gnome.shell favorite-apps "['firefox.desktop', 'org.gnome.Nautilus.desktop', 'libreoffice-startcenter.desktop']"

sudo tee /usr/local/bin/maintenance.sh << EOF > /dev/null
#!/usr/bin/bash

if [[ \${UID} == 0 ]] ; then
  echo 'Error: This script must not be ran with super-user privileges.'
  exit 2
fi

# Update the packages
sudo dnf update -y

# uBlock Origin's filters can't be automatically updated, so let's do it manually
notify-send --hint int:transient:1 'uBlock Origin' 'Purge all caches and update the filters.'
firefox

# Wait for Firefox to be closed before proceeding
wait \$(pidof firefox)
# As a safeguard, let's wait a bit more (otherwise some files fail to synchronise sometimes)
sleep 30

# Copy the content of the current session to /etc/skel
sudo rsync -avz --delete --exclude-from=/usr/local/share/rsync_ignore "\${HOME}/" /etc/skel
EOF

sudo tee /usr/local/share/rsync_ignore << EOF > /dev/null
.bash_history
.cache
.lock
.ssh
.sudo_as_admin_successful
bookmarks
lock
sessionstore-backups
Trash
EOF

# Let's make our script executable…
sudo chmod 755 /usr/local/bin/maintenance.sh
# … and let's run it for the first time
maintenance.sh

echo
echo
echo 'Setup finished, you may now reboot.'
echo
echo 'If you encounter any bug, please report them:'
echo '- https://codeberg.org/Silejonu/Public-facing_machine_with_home_loaded_in_RAM'
echo 'or'
echo '- silejonu@tutanota.com'
