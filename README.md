[ [Traduction française](https://codeberg.org/Silejonu/Public-facing_machine_with_home_loaded_in_RAM/src/branch/main/localisations/Fran%C3%A7ais) ]

# A guide to setup shared public-facing Linux machines with home loaded in RAM

In this guide, we'll learn how to setup a secure Linux system for public-facing machines. This is especially useful for local libraries, internet cafes, or any other place where a lot of users share the same computer and handle personal files.

# Summary

- [What are the advantages of this method?](#what-are-the-advantages-of-this-method)
- [How does it work?](#how-does-it-work)
	- [From the user's perspective](#from-the-user-s-perspective)
	- [From the administrator's perspective](#from-the-administrator-s-perspective)
	- [From a technical point-of-view](#from-a-technical-point-of-view)
- [Choice of the base distribution](#choice-of-the-base-distribution)
- [Base system installation](#base-system-installation)
- [System configuration](#system-configuration)
	- [Package management](#package-management)
	- [Setting up the user session](#setting-up-the-user-session)
	- [Workarounds](#workarounds)
	- [Privacy settings](#privacy-settings)
	- [Firefox configuration](#firefox-configuration)
	- [Desktop customisation](#desktop-customisation)
	- [Creation of the home directory template](#creation-of-the-home-directory-template)
		- [Improve the maintenance script](#improve-the-maintenance-script)
- [Go further](#go-further)

## What are the advantages of this method?

- no personal data is ever written to disk, making it disappear once the computer is turned off
- users can mess with their session as much as they want, it'll always reboot to the same template you created
- works on modestly specced machines
- little maintenance needed
- can be used by non-technical people
- easy to setup
- only uses basic Linux tools
- inexpensive

## How does it work?

### From the user's perspective

They turn the computer on, go about their business, download as many personal files, and login to as many personal accounts as they want. When they're done, they simply shut the computer down: they're not connected to any of their accounts anymore, and all their files are gone forever, with no possibility for anyone to steal them.

### From the administrator's perspective

You log into your administrator session, tweak the system to be just like you want your users to have it, and run `maintenance.sh`. All your tweaks will be applied to your users' sessions.  
All the changes your users do will be reverted once they shut the computer down. Whether they change the wallpaper, delete the entire Firefox profile, or set some ridiculous settings, you won't care, as everything will be reverted back to how you set it after a reboot!

### From a technical point-of-view

This method takes advantage of `/etc/skel` and `mkhomedir_helper`.  
You will create a generic user whose home directory is mounted to RAM (`/dev/shm`). Because of that, the content of `$HOME` will never reside on disk, and it won't survive a shut down.  
We will create a template in `/etc/skel`, which will be pushed by `mkhomedir_helper` to the user's home directory on each boot.

## Choice of the base distribution

In this guide, we'll see how to setup a [CentOS Stream](https://www.centos.org) machine. I chose it as the base because it's a stable distribution, that ships the latest Firefox ESR.

I prefer to use Firefox ESR for stability, and to enjoy the full potential of [enterprise policies](https://github.com/mozilla/policy-templates) (like setting the default search engine).

However, you are free to use any distribution you wish. With one caveat: Snaps will not work with our setup. If you're choosing Ubuntu, you'll have to replace the Snap packages of software you need with their .deb counterparts (for Firefox, for instance, you'll have to add a PPA or install it manually).

This guide will use commands for CentOS Stream 9. If you use another distribution, you will have to adapt some of them. That being said, the goals behind each command are clearly presented, and you should be able to adapt them to your distribution of choice with relative ease.

## Base system installation

- *Software Selection:* select the "Workstation" template.
- *Root Password:* skip it, we don't want a root user.
- *User Creation:* tick "Make this user administrator", and use a strong password. We'll call ours "Administrator".
- *Installation Destination:* create a boot partition (512 MB is more than enough) and mount it on `/boot/efi` for UEFI boot, or `/boot` for legacy boot. Then use the remaining space for the `/` partition. Choose EXT4 as the filesystem, and do not enable encryption. We don't need it since no user data will ever sit on the disk, and we want users to be able to turn the computer on without having to type a password. **Do not create a swap partition**: we don't want files being written to disk.

## System configuration

It is highly recommended you follow along this guide, and tweak things according to your needs. That being said, **if you've already read through it**, and are OK with the defaults, feel free to use [this automated configuration script](https://codeberg.org/Silejonu/Public-facing_machine_with_home_loaded_in_RAM/src/branch/main/Lazy%20installation%20script/install_public_facing_machine.sh).

### Package management

Let's enable a few repositories we'll need in the future.

```bash
# Enable CodeReady Linux Builder
sudo dnf config-manager --set-enabled crb
# Enable EPEL
sudo dnf install -y epel-release epel-next-release
# Enable RPM Fusion free and nonfree
sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %rhel).noarch.rpm
sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %rhel).noarch.rpm
# Update the system
sudo dnf upgrade -y
```

Now let's add a few programs that our users may want to use (adapt the list to your needs):

```bash
# Video codecs for Firefox
sudo dnf install -y ffmpeg gstreamer1-libav
# Multimedia packages needed by gstreamer enabled applications
sudo dnf groupupdate -y multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
# sound-and-video complement packages needed by some applications
sudo dnf groupupdate -y sound-and-video
# LibreOffice
sudo dnf install -y libreoffice-{calc,draw,impress,writer}
cp /usr/share/applications/libreoffice-startcenter.desktop ~/.local/share/applications/
sed -i '/NoDisplay=true/d' ~/.local/share/applications/libreoffice-startcenter.desktop
# GIMP
sudo dnf install -y gimp
# iOS devices filesystem support
sudo dnf install -y ifuse
# HEIF images support
sudo dnf install -y libheif
xdg-mime default org.gnome.eog.desktop image/heic
xdg-mime default org.gnome.eog.desktop image/heif
# WEBP images support
sudo dnf install -y webp-pixbuf-loader
xdg-mime default org.gnome.eog.desktop image/webp
```

Finally, let's remove a few packages that we won't need:

```bash
# Flatpak as we don't want users to install software themselves
sudo dnf remove -y flatpak
# A mail client serves no use on a shared machine
sudo dnf remove -y evolution
# We don't want automatic updates and we'll stick to dnf for package management
sudo dnf remove -y PackageKit*
# Cockpit is a server utility
sudo dnf remove -y cockpit
# Tour serves no use in our case
sudo dnf remove -y gnome-tour
```

### Setting up the user session

First, let's decide on our generic user's name:

```bash
fullname=User
username=user
```

```bash
# We create our generic user, with its home directory mounted in RAM
sudo useradd ${username} --home-dir /dev/shm/${username} --shell /bin/bash --comment "${fullname}"
```

There is no need for us to know our user password. Actually, it would be counter-productive, as logging back into a session would create a brand new home, as if we just installed our system, and ignore our home template (we'll talk about the home template later in this guide). So we will generate a random password, unknown to everybody (including us), and set up automatic login for the generic user. The only way to log into our generic user will be to reboot the computer. That's what we want.

```bash
# We generate a random password for the user
user_pw=$(printf "$(date)${RANDOM}" | md5sum | cut -d' ' -f1)
# We assign the password to our user
echo "${username}:${user_pw}" | sudo chpasswd
# We prevent anyone without sudo privileges to change their password
sudo chmod u-s /usr/bin/passwd
# We enable automatic login
sudo sed -i "/\[daemon\]/a AutomaticLogin=${username}" /etc/gdm/custom.conf
sudo sed -i "/\[daemon\]/a AutomaticLoginEnable=True" /etc/gdm/custom.conf
# We hide our user from the login screen
sudo tee /var/lib/AccountsService/users/${username} << EOF > /dev/null
[User]
SystemAccount=true
EOF
```

Now we need to create the service that will populate our user's home directory with the content of our template:

```bash
sudo tee /etc/systemd/system/mkhomedir_helper.service << EOF > /dev/null
[Unit]
Description=Run mkhomedir_helper in order to generate the home directory for the user "${username}" from the skel template.

[Service]
ExecStart=/usr/sbin/mkhomedir_helper ${username}

[Install]
WantedBy=graphical.target
EOF
```

SELinux will prevent `mkhomedir_helper` from writing in our user's home, so let's allow it by creating a custom rule:

```bash
# We create the rule template
sudo tee /tmp/custom_mkhomedir_helper.te << EOF > /dev/null
module custom_mkhomedir_helper 1.0;

require {
	type oddjob_mkhomedir_t;
	type tmpfs_t;
	class dir { create setattr };
	class file { create open setattr write };
}

#============= oddjob_mkhomedir_t ==============
allow oddjob_mkhomedir_t tmpfs_t:dir { create setattr };
allow oddjob_mkhomedir_t tmpfs_t:file { create open setattr write };
EOF
# We build the module
sudo checkmodule -M -m -o /tmp/custom_mkhomedir_helper.mod /tmp/custom_mkhomedir_helper.te
sudo semodule_package -m /tmp/custom_mkhomedir_helper.mod -o /tmp/custom_mkhomedir_helper.pp
# We install the module
sudo semodule -i /tmp/custom_mkhomedir_helper.pp
```

Now all that's left to do is to enable the service:

```bash
sudo systemctl enable mkhomedir_helper
```

### Workarounds

The following settings are needed to prevent users from locking themselves out of their session:

```bash
# Disable ability to put the computer to sleep
sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target
# Disable ability to lock the screen
gsettings set org.gnome.desktop.lockdown disable-lock-screen true
# Disable automatic screen lock
gsettings set org.gnome.desktop.screensaver lock-enabled false
# Disable blank screen timer
gsettings set org.gnome.desktop.session idle-delay 0
```

Since we don't have swap, it can be a good idea to compress files in RAM with zram:

```bash
sudo dnf install -y zram-generator
sudo tee /etc/systemd/zram-generator.conf << EOF > /dev/null
[zram0]
zram-size = ram / 2
compression-algorithm = zstd
swap-priority = 100
fs-type = swap
EOF
```

### Privacy settings

```bash
# Disable application usage history
gsettings set org.gnome.desktop.privacy remember-app-usage false
# Disable recent files
gsettings set org.gnome.desktop.privacy remember-recent-files false
# Automatically delete temporary files
gsettings set org.gnome.desktop.privacy remove-old-temp-files true
# Automatically delete old trash files
gsettings set org.gnome.desktop.privacy remove-old-trash-files true
# Set automatic deletion period to 1 day
gsettings set org.gnome.desktop.privacy old-files-age 1
```

### Firefox configuration

You will want to use [Firefox enterprise policies](https://github.com/mozilla/policy-templates) to improve security and tailor your users' experience.

```bash
# Let's create the Firefox directory that will host the policies
sudo mkdir -p /etc/firefox/policies
# And download the policies
sudo wget 'https://codeberg.org/Silejonu/Public-facing_machine_with_home_loaded_in_RAM/raw/branch/main/Configuration%20files/policies.json' --directory-prefix /etc/firefox/policies
```

I made [a template](https://codeberg.org/Silejonu/Public-facing_machine_with_home_loaded_in_RAM/src/branch/main/Configuration%20files/policies.json) that I believe most people would appreciate, but I recommend you read through the file before using it. At the very least, you should take a look at the localisation settings:

- add your locale's blocklist in uBlock Origin's settings (`selectedFilterLists`)
- add your locale to the beginning of `"RequestedLocales": "en-US,en"`
- add [a language pack](https://addons.mozilla.org/firefox/language-tools/) by following [these instructions](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#extensionsettings). Here is what it would look like for the French language pack, for instance:

```json
      "langpack-fr@firefox.mozilla.org": {
        "installation_mode": "force_installed",
        "install_url": "https://addons.mozilla.org/firefox/downloads/latest/français-language-pack/latest.xpi"
      },
```

I chose Startpage as the default search engine, because it shows anonymised Google results, and unlike Google, it does not lock your search behind a "Before you continue" banner. I found it to be particularly valuable on a system that does not save cookies. You can [edit its settings](https://www.startpage.com/do/settings) then copy the settings string in `URLTemplate`. It should look something like that:

```json
          "Name": "Startpage",
          "URLTemplate": "https://www.startpage.com/sp/search?query={searchTerms}&prfe=8d48ca6adae01eb75c25333c67841ceb81a522c0de4ff281a3d9405ccea07ab8d449902ec838dee3a9976bb0c676736d7ea3f0dc440f9f160da1d13091f58946f2565b014a377f9e300c62677d68&language=francais&t=device&lui=francais&cat=web&sc=4f9emuRQYhFa20&abp=-1",
          "IconURL": "https://www.startpage.com/sp/cdn/favicons/favicon--default.ico"
        }
```

I chose [Cloudflare for Families](https://developers.cloudflare.com/1.1.1.1/setup/#dns-over-https-doh) for DNS over HTTPS because it blocks domains providing malware and adult content, and it seems to me like what most people would want for public-facing machines. If all you care about is a privacy-respecting DNS, you can take a look at [Quad9](https://quad9.net).

You will probably also want to change the `URL` of `Homepage`, and maybe add a few sites to `ManagedBookmarks`.

uBlock Origin (and many other extensions) can be set up through `policies.json` as well. Take a look at [its documentation](https://github.com/gorhill/uBlock/wiki/Deploying-uBlock-Origin) for more information.

### Desktop customisation

The following configurations are not strictly needed, but I found them to be useful in my case, so I'll share them here:

```bash
# Delete the ~/Public directory
rmdir $(xdg-user-dir PUBLICSHARE)
# Show the weekday in the top bar
gsettings set org.gnome.desktop.interface clock-show-weekday true
# Set the favourites
gsettings set org.gnome.shell favorite-apps "['firefox.desktop', 'org.gnome.Nautilus.desktop', 'libreoffice-startcenter.desktop']"
```

### Creation of the home directory template

We're almost done! All we have to do now is to create the template that will get mounted in RAM when our generic user logs in.  
For that, we will create a script which will take care of all of the basic maintenance of our system:

```bash
sudo tee /usr/local/bin/maintenance.sh << EOF > /dev/null
#!/usr/bin/bash

if [[ \${UID} == 0 ]] ; then
  echo 'Error: This script must not be ran with super-user privileges.'
  exit 2
fi

# Update the packages
sudo dnf update -y

# uBlock Origin's filters can't be automatically updated, so let's do it manually
notify-send --hint int:transient:1 'uBlock Origin' 'Purge all caches and update the filters.'
firefox

# Wait for Firefox to be closed before proceeding
wait \$(pidof firefox)
# As a safeguard, let's wait a bit more (otherwise some files fail to synchronise sometimes)
sleep 30

# Copy the content of the current session to /etc/skel
sudo rsync -avz --delete --exclude-from=/usr/local/share/rsync_ignore "\${HOME}/" /etc/skel
EOF
```

Some files should not be added to the home template for privacy/security reasons, or simply because they would prevent some programs to work as intended. To make our life a bit easier, we'll create a blacklist for them. Edit it according to your needs, but here is a base:

```bash
sudo tee /usr/local/share/rsync_ignore << EOF > /dev/null
.bash_history
.cache
.lock
.ssh
.sudo_as_admin_successful
bookmarks
lock
sessionstore-backups
Trash
EOF
```

```bash
# Let's make our script executable…
sudo chmod 755 /usr/local/bin/maintenance.sh
# … and let's run it for the first time
maintenance.sh
```

We're done! You're ready to reboot to start enjoying your new system!

Don't forget to run `maintenance.sh` from time to time to update your install, and you're good to go!  
*Obviously you'll need to log as the user you created during the initial installation, as the generic user has no sudo privileges.*

#### Improve the maintenance script

The maintenance script we made is very basic. I recommend you improve it to automate a few more things, especially if you have a lot of machines. From experience, I recommend you setup a git repository, and store `policies.json` as well as `rsync_ignore` there. Then, add the following to `/usr/local/bin/maintenance.sh`:

```bash
sudo wget {URL_to_policies.json} --directory-prefix /etc/firefox/policies/
sudo wget {URL_to_rsync_ignore} --directory-prefix /usr/local/share/
```

Now, when you have to make a change to one of these files, you only have to make it once, in your git repository, and it will get applied to all your machines the next time you run `maintenance.sh`. This will significantly reduce the time you need to maintain your machines.

Similarly, you'll find yourself sometimes wanting to do small tweaks. Instead of going on each machine and running it, I recommend you make a `tweaks.sh` file in your git repository, and add the following to the beginning of `/usr/local/bin/maintenance.sh`:

```bash
wget {URL_to_tweaks.sh} --directory-prefix /tmp/
bash /tmp/tweaks.sh
```

From now on, when you need to tweak your system, all you have to do is to add the commands of your choice to `tweaks.sh` in your git repository, and they'll get run alongside `maintenance.sh`.

## Go further

Here are a few things you will probably want to do:

- [install the Microsoft fonts](https://codeberg.org/Silejonu/miscellaneous_scripts/src/branch/main/02.%20Linux/install_microsoft_fonts.sh), if your users need to edit documents
- change the wallpaper for something informative (for instance, you could explain that files are automatically flushed after a shutdown)
- add DVD support (`sudo dnf install rpmfusion-free-release-tainted && sudo dnf install libdvdcss`)
